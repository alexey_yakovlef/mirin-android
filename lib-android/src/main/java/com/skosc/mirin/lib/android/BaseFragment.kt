package com.skosc.mirin.lib.android

import android.content.Context
import androidx.fragment.app.Fragment
import org.kodein.di.KodeinAware

open class BaseFragment : Fragment(), KodeinAware {
    override val kodein by lazy { (activity as KodeinAware).kodein }
    open val screenStyle: ScreenStyle = ScreenStyle.NAVIGATION

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        applyStyleToActivityIfPossible()
    }

    fun applyStyleToActivityIfPossible() {
        val currentActivity = activity
        if (currentActivity is ScreenStyleApplicator) {
            currentActivity.applyScreenStyle(screenStyle)
        }
    }
}