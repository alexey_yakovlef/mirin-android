package com.skosc.mirin.lib.android

interface ScreenStyleApplicator {
    fun applyScreenStyle(style: ScreenStyle)
}