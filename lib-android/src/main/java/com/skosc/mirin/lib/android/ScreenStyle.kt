package com.skosc.mirin.lib.android

enum class ScreenStyle(val hideToolbar: Boolean, val hideBottombar: Boolean) {
    FULL_SCREEN(true, true),
    NAVIGATION(false, false),
    DETAIL(false, true)
}