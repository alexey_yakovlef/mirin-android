package com.skosc.mirin.lib.android

import androidx.appcompat.app.AppCompatActivity
import org.kodein.di.KodeinAware

open class BaseActivity : AppCompatActivity(), KodeinAware {
    override val kodein by lazy { (application as KodeinAware).kodein }
}
