package com.skosc.mirin.domain.security.repo

import com.skosc.mirin.domain.schedule.entity.GroupName

interface CurrentUserRepository {
    suspend fun getPrimaryGroup(): GroupName?
    suspend fun setPrimaryGroup(group: GroupName)
}