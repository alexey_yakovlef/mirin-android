package com.skosc.mirin.domain.security.repo

import com.skosc.mirin.domain.schedule.entity.GroupName
import com.skosc.mirin.lib.kvs.MutableKVStorage

class CurrentUserRepositoryImpl(private val kvs: MutableKVStorage): CurrentUserRepository {
    companion object {
        private const val KEY_PRIMARY_GROUP_NAME = "curi_primary_group_name"
    }

    override suspend fun getPrimaryGroup(): GroupName? {
        return kvs.getString(KEY_PRIMARY_GROUP_NAME)
    }

    override suspend fun setPrimaryGroup(group: GroupName) {
        kvs.setString(KEY_PRIMARY_GROUP_NAME, group)
    }

}