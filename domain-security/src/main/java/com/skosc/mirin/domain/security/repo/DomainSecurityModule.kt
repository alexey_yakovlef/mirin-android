package com.skosc.mirin.domain.security.repo

import android.content.Context
import com.skosc.mirin.lib.di.DefaultModule
import com.skosc.mirin.lib.kvs.KVSType
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val ModuleDomainSecurity = DefaultModule("domain-security") {
    bind<CurrentUserRepository>() with singleton {
        val context = instance<Context>()
        val sharedPreferences = context.getSharedPreferences("current_user", Context.MODE_PRIVATE)
        return@singleton CurrentUserRepositoryImpl(instance(tag=KVSType.PERSISTENT, arg=sharedPreferences))
    }
}