package com.skosc.mirin.core

typealias EventHandler<T> = (T) -> Unit

class Event<T> {
    private val handlers: MutableList<EventHandler<T>> = mutableListOf()

    fun subscribe(handler: EventHandler<T>) {
        handlers.add(handler)
    }

    fun fire(argument: T) {
        handlers.forEach { it.invoke(argument) }
    }

    operator fun plusAssign(handler: EventHandler<T>) = subscribe(handler)
}