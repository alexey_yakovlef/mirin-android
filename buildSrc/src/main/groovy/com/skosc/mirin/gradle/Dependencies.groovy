package com.skosc.mirin.gradle

class Dependencies {
    static class Kotlin {
        public static final def STD_LIB = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.3.31"

        static class Coroutines {
            public static final def CORE = 'org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.1'
            public static final def ANDROID = 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.2.1'
        }
    }

    static class AndroidX {
        public static final def CORE_KTX = 'androidx.core:core-ktx:1.0.2'
        public static final def LIFECYCLE = 'androidx.lifecycle:lifecycle-extensions:2.0.0'
        public static final def MATERIAL = 'com.google.android.material:material:1.0.0'

        static class AppCompat {
            public static final def APP_COMPAT = "androidx.appcompat:appcompat:1.0.2"
        }

        static class UI {
            public static final def CONSTRAINT_LAYOUT = 'androidx.constraintlayout:constraintlayout:2.0.0-alpha3'
            public static final def RECYCLER = 'androidx.recyclerview:recyclerview:1.1.0-alpha06'
            public static final def CARD_VIEW = 'androidx.cardview:cardview:1.0.0'
        }

        static class Navigation {
            public static final def UI = 'androidx.navigation:navigation-ui:2.0.0'
            public static final def UI_KTX = 'androidx.navigation:navigation-ui-ktx:2.0.0'
            public static final def FRAGMENT = 'androidx.navigation:navigation-fragment:2.0.0'
            public static final def FRAGMENT_KTX = 'androidx.navigation:navigation-fragment-ktx:2.0.0'
        }
    }

    static class Mirin {
        static class Lib {
            public static final def CORE = ":core"
            public static final def DI = ":lib-di"
            public static final def FIREBASE = ":lib-firebase"
            public static final def ANDROID = ":lib-android"
            public static final def NAVIGATION = ":lib-navigation"
            public static final def MVVM = ":lib-mvvm"
            public static final def UI = ":lib-ui"
            public static final def KVS = ":lib-kvs"
        }

        static class Feature {
            public static final def MAIN = ":feature-main"
            public static final def GROUP_SEARCH = ':feature-groupsearch'
            public static final def DASHBOARD = ':feature-dashboard'
            public static final def SCHEDULE = ':feature-schedule'
            public static final def SETTINGS = ':feature-settings'
        }

        static class Domain {
            public static final def SCHEDULE = ':domain-schedule'
            public static final def SECURITY = ':domain-security'
        }
    }

    static class Firebase {
        public static final def CORE = 'com.google.firebase:firebase-core:16.0.9'
        public static final def FIRESTORE = 'com.google.firebase:firebase-firestore:19.0.0'
        public static final def FIRESTORE_KTX = 'com.google.firebase:firebase-firestore-ktx:19.0.0'
    }

    public static final def KODEIN = 'org.kodein.di:kodein-di-generic-jvm:6.2.1'
    public static final def JODA = 'joda-time:joda-time:2.10.2'
    public static final def DECORO = 'ru.tinkoff.decoro:decoro:1.3.5'

    public static final def GLIDE = 'com.github.bumptech.glide:glide:4.9.0'
    public static final def GLIDE_COMPILER = 'com.github.bumptech.glide:compiler:4.9.0'
}