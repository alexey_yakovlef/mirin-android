package com.skosc.mirin.feature.groupsearch.ui


import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skosc.mirin.feature.groupsearch.GroupSearchViewModel
import com.skosc.mirin.feature.groupsearch.R
import com.skosc.mirin.feature.groupsearch.misc.GroupNameInputFormatter
import com.skosc.mirin.feature.groupsearch.model.GroupUIModel
import com.skosc.mirin.lib.android.BaseFragment
import com.skosc.mirin.lib.android.ScreenStyle
import com.skosc.mirin.lib.mvvm.viewModelProvider
import com.skosc.mirin.lib.navigation.Navigator
import com.skosc.mirin.lib.navigation.findNavigator
import kotlinx.android.synthetic.main.feature_groupsearch_fragment_group_search.*


class GroupSearchFragment : BaseFragment() {
    private val vm by viewModelProvider(GroupSearchViewModel::class)
    private val groupNameFormatter: GroupNameInputFormatter = GroupNameInputFormatter()
    private val navigator: Navigator by lazy { findNavigator() }

    override val screenStyle: ScreenStyle = ScreenStyle.FULL_SCREEN

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.feature_groupsearch_fragment_group_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupSearchBar()
    }

    private fun setupSearchBar() {
        val suggestionsAdapter = GroupSuggestRecyclerAdapter()
        suggestionsAdapter.onItemClickedEvent += ::onGroupClicked
        group_suggestions.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        group_suggestions.adapter = suggestionsAdapter
        group_suggestions.addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
        groupNameFormatter.attach(search_bar.queryEdit)
        search_bar.queryEdit.filters = arrayOf(InputFilter.AllCaps())
        search_bar.onQueryChangedEvent.subscribe { query ->
            vm.search(query).observe(this, Observer {
                suggestionsAdapter.submit(it)
            })
        }
    }

    private fun onGroupClicked(groupUIModel: GroupUIModel) {
        vm.selectGroup(groupUIModel.name).observe(this) {
            navigator.navigate(R.id.navigation_dashboard)
        }
    }
}
