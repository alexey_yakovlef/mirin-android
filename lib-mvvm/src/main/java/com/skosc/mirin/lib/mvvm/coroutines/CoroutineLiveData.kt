package com.skosc.mirin.lib.mvvm.coroutines

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.skosc.mirin.lib.mvvm.LiveEvent
import kotlinx.coroutines.*

fun <T> CoroutineScope.launchLiveData(
    dispatcher: CoroutineDispatcher = Dispatchers.IO,
    supplier: suspend () -> T
): LiveData<T> {
    val liveData = MutableLiveData<T>()

    launch(dispatcher) {
        liveData.postValue(supplier.invoke())
    }

    return liveData
}

fun <T> CoroutineScope.launchLiveEvent(
    dispatcher: CoroutineDispatcher = Dispatchers.IO,
    action: suspend () -> T
): LiveEvent {
    val event = LiveEvent()

    launch(dispatcher) {
        action.invoke()
        event.fire()
    }

    return event
}