package com.skosc.mirin.lib.mvvm

import androidx.lifecycle.*

class LiveEvent : MutableLiveData<Int>() {
    companion object {
        fun fire(): LiveEvent {
            val event = LiveEvent()
            event.fire()
            return event
        }
    }

    fun fire() {
        this.postValue((value ?: 0) + 1)
    }

    fun observe(owner: LifecycleOwner, observer: () -> Unit) {
        this.observe(owner, Observer { observer.invoke() })
    }

}