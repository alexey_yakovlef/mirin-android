package com.skosc.mirin.lib.mvvm

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import org.kodein.di.KodeinAware
import org.kodein.di.direct
import org.kodein.di.generic.instance
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

class KodeinViewModelProviderDelegate<VM : ViewModel>(private val cls: KClass<VM>) {
    operator fun getValue(thisRef: KodeinAware, property: KProperty<*>): VM {
        val factory = thisRef.kodein.direct.instance<ViewModelProvider.Factory>(
            tag = cls
        )

        val provider = when (thisRef) {
            is FragmentActivity -> ViewModelProviders.of(thisRef, factory)
            is Fragment -> ViewModelProviders.of(thisRef, factory)
            else -> throw IllegalArgumentException("This delegate only supports Activities and Fragments")
        }

        return provider.get(cls.java)
    }
}

fun <T : ViewModel> viewModelProvider(cls: KClass<T>): KodeinViewModelProviderDelegate<T> {
    return KodeinViewModelProviderDelegate(cls)
}