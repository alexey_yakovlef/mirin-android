package com.skosc.mirin.lib.ui.widget

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

/**
 * Generic implementation of [DiffUtil.ItemCallback] using [equals] and reference comparison
 */
class GenericDiffCallback<T> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean = oldItem === newItem
    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean = oldItem == newItem
}