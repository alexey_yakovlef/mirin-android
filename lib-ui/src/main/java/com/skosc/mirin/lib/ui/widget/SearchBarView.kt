package com.skosc.mirin.lib.ui.widget

import android.content.Context
import android.text.Editable
import android.util.AttributeSet
import android.widget.EditText
import androidx.cardview.widget.CardView
import com.skosc.mirin.core.Event
import com.skosc.mirin.lib.android.listener.SimpleTextWatcher
import com.skosc.mirin.lib.ui.R
import kotlinx.android.synthetic.main.lib_ui_view_search_bar.view.*

class SearchBarView @JvmOverloads constructor(
    context: Context, attrSet: AttributeSet? = null, defStyleAttr: Int = 0
) : CardView(context, attrSet, defStyleAttr) {
    val onQueryChangedEvent = Event<String>()

    val queryEdit: EditText get() = query_edit

    init {
        inflate(context, R.layout.lib_ui_view_search_bar, this)
        query_edit.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(editable: Editable?) {
                editable?.let { onQueryChangedEvent.fire(it.toString()) }
            }
        })
    }

}