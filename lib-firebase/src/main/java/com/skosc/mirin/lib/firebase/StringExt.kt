package com.skosc.mirin.lib.firebase

import java.util.*

val String.normalizeAsGroupName get() = trim().toUpperCase(Locale.getDefault())