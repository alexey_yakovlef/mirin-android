package com.skosc.mirin.lib.kvs

interface MutableKVStorage : KVStorage {
    suspend fun setString(key: String, value: String)
}