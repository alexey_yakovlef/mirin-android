package com.skosc.mirin.lib.kvs

import android.content.SharedPreferences

class PreferenceKVStorage(private val sharedPrefs: SharedPreferences): MutableKVStorage {

    override suspend fun setString(key: String, value: String) {
        sharedPrefs.edit()
            .putString(key, value)
            .apply()
    }

    override suspend fun getString(key: String): String? = sharedPrefs.getString(key, null)

}