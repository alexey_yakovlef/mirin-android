package com.skosc.mirin.lib.kvs

enum class KVSType {
    IN_MEMORY, PERSISTENT
}