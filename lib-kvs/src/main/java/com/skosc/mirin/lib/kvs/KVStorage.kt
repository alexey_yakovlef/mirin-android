package com.skosc.mirin.lib.kvs

interface KVStorage {
    suspend fun getString(key: String): String?
}