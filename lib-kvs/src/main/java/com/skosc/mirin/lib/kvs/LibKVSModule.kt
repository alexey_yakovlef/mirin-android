package com.skosc.mirin.lib.kvs

import android.content.SharedPreferences
import com.skosc.mirin.lib.di.DefaultModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.factory

val ModuleLibKVS = DefaultModule("lib-kvs") {
    bind<MutableKVStorage>(KVSType.PERSISTENT) with factory { sp: SharedPreferences -> PreferenceKVStorage(sp) }
}