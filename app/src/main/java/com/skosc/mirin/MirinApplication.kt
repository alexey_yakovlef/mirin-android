package com.skosc.mirin

import android.app.Application
import android.content.Context
import com.skosc.mirin.domain.schedule.ModuleFeatureSchedule
import com.skosc.mirin.domain.security.repo.ModuleDomainSecurity
import com.skosc.mirin.feature.groupsearch.ModuleFeatureGroupSearch
import com.skosc.mirin.lib.firebase.FirebaseIgnite
import com.skosc.mirin.lib.kvs.ModuleLibKVS
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance

class MirinApplication : Application(), KodeinAware {
    override val kodein: Kodein = Kodein.lazy {
        bind<Context>() with instance(this@MirinApplication)
        importOnce(FirebaseIgnite(this@MirinApplication).ignite())
        importOnce(ModuleFeatureSchedule)
        importOnce(ModuleFeatureGroupSearch)
        importOnce(ModuleLibKVS)
        importOnce(ModuleDomainSecurity)
    }

}