package com.skosc.mirin.lib.navigation

import androidx.fragment.app.Fragment

fun Fragment.findNavigator(): Navigator = JetPackNavigator(this)