package com.skosc.mirin.domain.schedule

import com.skosc.mirin.domain.schedule.repo.GroupIndexRepo
import com.skosc.mirin.lib.di.DefaultModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

val ModuleFeatureSchedule = DefaultModule("feature_di") {
    bind<GroupIndexRepo>() with provider { GroupIndexRepo(instance()) }
}