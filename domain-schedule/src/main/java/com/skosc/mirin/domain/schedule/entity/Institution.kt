package com.skosc.mirin.domain.schedule.entity

import androidx.annotation.ColorRes
import com.skosc.mirin.domain.schedule.R
import com.skosc.mirin.lib.firebase.normalizeAsGroupName

enum class Institution(@ColorRes val colorRes: Int) {
    IEP(R.color.domain_schedule_institution_iep),
    IIT(R.color.domain_schedule_institution_iit),
    INTEGU(R.color.domain_schedule_institution_integu),
    ITHT(R.color.domain_schedule_institution_itht),
    KBSP(R.color.domain_schedule_institution_kbsp),
    KIB(R.color.domain_schedule_institution_kib),
    RTS(R.color.domain_schedule_institution_rts),
    FTI(R.color.domain_schedule_institution_fti),
    UNKNOWN(0);

    companion object {
        fun fromGroupName(group: String): Institution {
            val normalized = group.normalizeAsGroupName
            return when {
                normalized.startsWith("Т") -> FTI
                normalized.startsWith('И') -> IIT
                normalized.startsWith("Г") -> INTEGU
                normalized.startsWith("К") -> KIB
                normalized.startsWith("Б") -> IEP
                normalized.startsWith("Р") -> RTS
                normalized.startsWith("Х") -> ITHT
                else -> UNKNOWN
            }
        }
    }
}