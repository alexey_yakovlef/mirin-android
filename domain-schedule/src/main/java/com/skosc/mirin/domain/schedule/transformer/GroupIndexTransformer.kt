package com.skosc.mirin.domain.schedule.transformer

import com.google.firebase.firestore.DocumentSnapshot
import com.skosc.mirin.domain.schedule.entity.GroupIndex

object GroupIndexTransformer: (DocumentSnapshot) -> GroupIndex {
    override fun invoke(snapshot: DocumentSnapshot): GroupIndex {
        val data = snapshot.data!!
        return GroupIndex(
            term = TermTransformer.invoke(data["term"] as Map<String, Any>),
            groups = data["available_groups"] as List<String>
        )
    }
}