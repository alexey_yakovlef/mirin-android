package com.skosc.mirin.domain.schedule.entity

import org.joda.time.DateTime

class Term(
    val year: Int,
    val semester: Int
) {
    companion object {
        val current
            get() : Term {
                val now = DateTime.now()
                val isBeginningOfTerm = now.withYear(0) >= DateTime.parse("0-09-01")
                val currentYear = now.year
                return if (isBeginningOfTerm) {
                    Term(year = currentYear, semester = 1)
                } else {
                    Term(year = currentYear - 1, semester = 2)
                }
            }
    }

    val key get() = "$year-$semester"
}