package com.skosc.mirin.domain.schedule.transformer

import com.skosc.mirin.domain.schedule.entity.Term

object TermTransformer : (Map<String, Any>) -> Term {
    override fun invoke(snapshot: Map<String, Any>): Term = Term(
        year = (snapshot["year"] as Long).toInt(),
        semester = (snapshot["semester"] as Long).toInt()
    )
}

