package com.skosc.mirin.lib.di

import org.kodein.di.Kodein

private const val DEFAULT_KODEIN_MODULE_PREFIX = "mirin_dfm_"

fun DefaultModule(name: String, builder: Kodein.Builder.() -> Unit): Kodein.Module {
    return Kodein.Module(
        name = name,
        allowSilentOverride = false,
        prefix = DEFAULT_KODEIN_MODULE_PREFIX,
        init = builder
    )
}